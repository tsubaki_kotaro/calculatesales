package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	
	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";
	
	// 支店コードの定義
	private static final String SHOP_FILE_NAME_DEFINITION = "^\\d{3}$";
	
	// 商品コードの定義
	private static final String COMMODITY_FILE_NAME_DEFINITION = "^[a-zA-Z0-9]{8}$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NAME_CONTINUOS_NOT = "売上ファイル名が連番になっていません";
	private static final String TOTAL_SALES_AMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String SALES_FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String SHOP_CODE_INVALID = "の支店コードが不正です";
	private static final String COMMODITY_CODE_INVALID = "の商品コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が渡されていないときのエラー
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> branchCommodityNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> branchCommoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, SHOP_FILE_NAME_DEFINITION, "支店")) {
			return;
		}
		
		//商品定義ファイルの読込処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, branchCommodityNames, branchCommoditySales, COMMODITY_FILE_NAME_DEFINITION, "商品")) {
			return;
		}

		//すべてのファイルを取得
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//売上ファイルのパスのみを選別しListに格納
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^\\d{8}\\.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//すべての売上ファイル名が連番になっていないときのエラー
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if(latter - former != 1) {
				System.out.println(FILE_NAME_CONTINUOS_NOT);
				return;
			}
		}

		BufferedReader br = null;

		//売上ファイルをMapに格納して売上額を合計する
		try {
			//売上ファイルを一つずつ読み込んで売上額を合計する
			for(int i = 0; i < rcdFiles.size(); i++) {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				List<String> fileDate = new ArrayList<>();

				String line;
				String fileName = rcdFiles.get(i).getName();
				//売上ファイルの中身をListに格納する[支店コード(0), 売上額(1)]
				while((line = br.readLine()) != null) {
					fileDate.add(line);
				}

				String shopCode = fileDate.get(0);
				String commodityCode = fileDate.get(1);
				String shopSales = fileDate.get(2);

				//売上ファイルのフォーマットが不正だった時のエラー
				if(fileDate.size() != 3) {
					System.out.println(fileName + SALES_FILE_INVALID_FORMAT);
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルにない場合のエラー
				if(!branchNames.containsKey(shopCode)) {
					System.out.println(fileName + SHOP_CODE_INVALID);
					return;
				}
				
				//売上ファイルの商品コードが商品定義ファイルにない場合のエラー
				if(!branchCommodityNames.containsKey(commodityCode)) {
					System.out.println(fileName + COMMODITY_CODE_INVALID);
					return;
				}

				//売上ファイルの売上金額が数字でない場合のエラー
				if(!shopSales.matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(shopSales);

				//支店ごとの売上を合計する
				long shopSaleAmount = branchSales.get(shopCode) + fileSale;
				branchSales.replace(shopCode, shopSaleAmount);
				//商品ごとの売上を合計する
				long commoditySaleAmount = branchCommoditySales.get(commodityCode) + fileSale;
				branchCommoditySales.replace(commodityCode, commoditySaleAmount);

				//一店舗or一商品ごとの合計売上額が10桁を超えた場合のエラー
				if(shopSaleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println(TOTAL_SALES_AMOUNT_OVER);
					return;
				}
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, branchCommodityNames, branchCommoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイルor商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param [支店コードと支店名]or[商品コードと商品名]を保持するMap
	 * @param [支店コードと売上金額]or[商品コードと売上金額]を保持するMap
	 * @param ファイル名定義
	 * @param ファイルの種類
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales, final String FILE_NAME_DEFINITION, final String FILE_NAME) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイルが存在しない場合のエラー
			if(!file.exists()) {
				System.out.println(FILE_NAME + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				String shopCode = items[0];
				String shopName = items[1];

				//ファイルのフォーマットが不正だった時のエラー
				if((items.length != 2) || (!shopCode.matches(FILE_NAME_DEFINITION))) {
					System.out.println(FILE_NAME + FILE_INVALID_FORMAT);
					return false;
				}

				//ファイルの中身をMapに格納する（支店コード, 支店名）or（商品コード, 商品名）
				branchNames.put(shopCode, shopName);
				//ファイルの中身をMapに格納する
				//（支店コード, 初期化された売上金額（0円））or（商品コード, 初期化された売上金額（0円））
				branchSales.put(shopCode, 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイルor商品別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param [支店コードと支店名]or[商品コードと商品名]を保持するMap
	 * @param [支店コードと売上金額]or[商品コードと売上金額]を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//forで支店コードor商品コード(key)に合わせた支店名or商品名と総売上を一行ずつ書き出す
			//出力イメージ：[支店コード,支店名,合計売上額\r\n]or[商品コード,商品名,合計売上額\r\n]
			for (String key : branchNames.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					//ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
